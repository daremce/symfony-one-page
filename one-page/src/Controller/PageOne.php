<?php
// src/Controller/PageOne.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class PageOne
{
    public function index()
    {
        return new Response(
            '<html><body><h1>HELLO WORLD</h1></body></html>'
        );
    }
}
